import { Button, Row, Col, Divider } from 'antd'

import List from 'components/List'
import ModalForm from 'components/ModalForm'

import styles from './styles.module.scss'

const TodoView = ({
  data,
  handleDelete,
  handleUpdate,
  handleNewTodo,
}) => {
  return (
    <div className="container">
      <ModalForm />
      <Button type="primary" onClick={handleNewTodo}>Create New Todo</Button>
      <Row gutter={16}>
        <Col className="gutter-row" span={12}>
          <Divider orientation="left">To do list</Divider>
          <div className={styles.box}>
            <List
              data={data.todo}
              handleDelete={handleDelete}
              handleUpdate={handleUpdate}
            />
          </div>
        </Col>
        <Col className="gutter-row" span={12}>
          <Divider orientation="left">Done</Divider>
          <div className={styles.box}>
            <List
              data={data.done}
              handleDelete={handleDelete}
              handleUpdate={handleUpdate}
            />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default TodoView