import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { todoFetch, todoSetField } from 'redux/ducks/todo'

import TodoView from './TodoView'

const TodoContainer = () => {
  const dispatch = useDispatch()
  const todo = useSelector((state) => state.todo)

  const { data } = todo

  useEffect(() => {
    dispatch(todoFetch())
  }, [dispatch])

  const handleNewTodo = () => {
    dispatch(todoSetField({
      isModalForm: true,
    }))
  }

  const handleUpdate = (payload) => {
    dispatch(todoSetField({
      isModalForm: true,
      selected: payload,
    }))
  }

  const handleDelete = (payload) => {
    const type = payload.status > 0 ? 'done' : 'todo'

    dispatch(todoSetField({
      data: {
        ...data,
        [type]: data[type].filter((row) => row.id !== payload.id)
      }
    }))
  }

  const props = {
    data,
    handleUpdate,
    handleDelete,
    handleNewTodo,
  }

  return <TodoView {...props} />
}

export default TodoContainer