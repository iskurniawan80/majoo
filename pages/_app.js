import '../styles/globals.scss'
import 'antd/dist/antd.css'

import { Provider } from 'react-redux'
import configureStore from '../redux'

function MyApp({ Component, pageProps }) {

  const initialState = {}
  const store = configureStore(initialState)

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
