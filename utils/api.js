import { ajax } from 'rxjs/ajax'
import { map, catchError } from 'rxjs/operators'
import { throwError } from 'rxjs'

export default function api(options = {}) {
  const {
    endpoint = ['get', ''],
  } = options
  const [method, path] = endpoint
  const apiHost = 'https://virtserver.swaggerhub.com'
  const url = `${apiHost}/${path}`

  return ajax({
    method,
    url,
    headers: {
      'Content-Type': 'application/json',
    },
  }).pipe(
    map(res => res.response || {}),
    catchError((error) =>  throwError(error.response)),
  )
}
