export const sorting = (array, sorting = 'asc') => {
  const isAsc = sorting === 'asc' ? true : false

  const compare = (a, b) => {
    if ( a.createdAt < b.createdAt ){
      return isAsc ? -1 : 1
    }
    if ( a.createdAt > b.createdAt ){
      return isAsc ? 1 : -1
    }
    return 0;
  }

  return array.sort(compare)
}

export default sorting