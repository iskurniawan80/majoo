import { combineReducers } from 'redux'
import todo from '../ducks/todo'

const appReducers = combineReducers({
  todo,
})

const rootReducers = (state, action) => {
  return appReducers(state, action)
}

export default rootReducers
