import { combineEpics } from 'redux-observable'

import * as todoEpics from '../epics/todo'

function rootEpics(...args) {

  const allEpics = [
    ...Object.values(todoEpics),
  ]

  return combineEpics(...allEpics)(...args)
}

export default rootEpics
