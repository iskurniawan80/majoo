import createReducer from 'utils/createReducer'

export const TODO_FETCH = 'TODO_FETCH'
export const TODO_FETCH_SUCCESS = 'TODO_FETCH_SUCCESS'
export const TODO_FETCH_FAILED = 'TODO_FETCH_FAILED'

export const TODO_SET_FIELD = 'TODO_SET_FIELD'

export const INITIAL_STATE = {
  data: {
    todo: [],
    done: [],
  },
  selected: {},
  isModalForm: false,
  isLoading: false,
  isError: false,
  errorMessage: '',
}

const reducer = createReducer(INITIAL_STATE, {
  [TODO_FETCH]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [TODO_FETCH_SUCCESS]: (state, payload) => ({
    ...state,
    isLoading: false,
    data : payload,
  }),
  [TODO_FETCH_FAILED]: (state, payload) => ({
    ...state,
    isLoading: false,
    isError: true,
    errorMessage: payload,
  }),

  [TODO_SET_FIELD]: (state, payload) => ({
    ...state,
    ...payload,
  }),
})

export const todoFetch = () => ({
  type: TODO_FETCH,
})
export const todoFetchSuccess = (payload) => ({
  type: TODO_FETCH_SUCCESS,
  payload,
})
export const todoFetchFailed = (payload) => ({
  type: TODO_FETCH_FAILED,
  payload,
})

export const todoSetField = (payload) => ({
  type: TODO_SET_FIELD,
  payload,
})

export default reducer