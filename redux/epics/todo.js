import { ofType } from 'redux-observable'
import { of } from 'rxjs'
import {
  map,
  mergeMap,
  catchError
} from 'rxjs/operators'

import {
  TODO_FETCH,
  todoFetchSuccess,
  todoFetchFailed,
} from '../ducks/todo'

import { TODO_GET } from 'config/endpoint'
import Sorting from 'utils/sorting'

export function todoFetchEpic(action$, state$, { api }) {
  return action$.pipe(
    ofType(TODO_FETCH),
    mergeMap(() => api({
      endpoint: TODO_GET,
    }).pipe(
      map(res => {
        let todo = []
        let done = []

        res.map((data) => {
          if (data.status === 0) {
            todo.push(data)
          } else {
            done.push(data)
          }
        })

        return todoFetchSuccess({
          todo: Sorting(todo, 'asc'),
          done: Sorting(done, 'desc'),
        })
      }),
      catchError(() => of(todoFetchFailed('Gagal Fetching Data Todo!'))),
    )),
  )
}
