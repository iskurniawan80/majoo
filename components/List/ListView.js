import { Button, List, Typography } from 'antd'

import styles from './styles.module.scss'

const ListView = ({
  data,
  handleDelete,
  handleUpdate,
}) => {
  return (
    <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={item => (
        <List.Item
          actions={[
            <Button type="primary" onClick={() => handleUpdate(item)}>Update</Button>,
            <Button type="primary" danger onClick={() => handleDelete(item)}>Delete</Button>,
          ]}
        >
          <List.Item.Meta
            avatar="..."
            title={item.title}
            description={
              <>
                <Typography className={styles.description}>{item.description}</Typography>
                <Typography className={styles.date}>{item.createdAt}</Typography>
              </>
            }
          />
        </List.Item>
      )}
    />
  )
}

export default ListView