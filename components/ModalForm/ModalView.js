import { Modal, Input, Switch, Typography } from 'antd'

import styles from './styles.module.scss'

const { TextArea } = Input

const ModalView = ({
  form,
  title,
  isModalForm,
  handleOk,
  handleCancel,
  handleForm,
}) => {
  return (
    <Modal
      title={title}
      visible={isModalForm}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Typography className={styles.label}>Title</Typography>
      <Input
        value={form.title}
        placeholder="Title Todo"
        onChange={(e) => handleForm('title', e.target.value)}
      />
      <Typography className={styles.label}>Description</Typography>
      <TextArea
        value={form.description}
        placeholder="Description Todo"
        onChange={(e) => handleForm('description', e.target.value)}
      />
      <Typography className={styles.label}>Status</Typography>
      <Switch
        checked={form.status > 0 ? true : false}
        onChange={(e) => handleForm('status', !!e ? 1 : 0)}
      />
    </Modal>
  )
}

export default ModalView