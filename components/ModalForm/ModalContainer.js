import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import dayjs from 'dayjs'

import { todoSetField } from 'redux/ducks/todo'
import { generateId } from 'utils/generateId'
import Sorting from 'utils/sorting'

import ModalView from './ModalView'

const defaultTitle = 'Create Todo'
const defaultForm = {
  id: generateId(),
  title: '',
  description: '',
  status: 0,
}

const TodoContainer = () => {
  const dispatch = useDispatch()
  const todo = useSelector((state) => state.todo)
  const { selected, isModalForm, data } = todo

  const [title, setTitle] = useState(defaultTitle)
  const [isNewTodo, setIsNewTodo] = useState(true)
  const [form, setForm] = useState(defaultForm)

  useEffect(() => {
    if (selected.id) {
      setTitle(`Update Todo: ${selected.title}`)
      setIsNewTodo(false)
      setForm(selected)
    } else {
      setTitle(defaultTitle)
      setIsNewTodo(true)
      setForm(defaultForm)
    }
  }, [selected])

  const handleForm = (field, value) => {
    setForm({
      ...form,
      [field]: value,
    })
  }

  const handleCancel = () => {
    dispatch(todoSetField({
      isModalForm: false,
      selected: {},
    }))
  }

  const handleOk = () => {
    const sort = form.status > 0 ? 'desc' : 'asc'
    const currentType = selected.status > 0 ? 'done' : 'todo'
    const updatedType = form.status > 0 ? 'done' : 'todo'
    let updateData = data

    if (isNewTodo) {
      const formValue = {
        ...form,
        createdAt: dayjs().format('YYYY-MM-DD HH:mm:ss'),
      }
      
      let todoList = data[updatedType]
      todoList.push(formValue)
      todoList = Sorting(todoList, sort)
      updateData = {
        ...data,
        [updatedType]: todoList,
      }
    } else {
      updateData = {
        ...updateData,
        [currentType]: data[currentType].filter((row) => row.id !== selected.id),
      }
      
      let newTodo = updateData[updatedType]
      newTodo.push(form)

      updateData = {
        ...updateData,
        [updatedType]: Sorting(newTodo, sort)
      }
    }

    dispatch(todoSetField({
      data: updateData,
      isModalForm: false,
      selected: {},
    }))
  }

  const props = {
    form,
    title,
    isModalForm,
    handleOk,
    handleCancel,
    handleForm,
  }

  return <ModalView {...props} />
}

export default TodoContainer